# Query Auto Completion for rare prefixes using Learn-to-rank i.e. LambdaMart model.

## Steps:
-   Run the `Rare_query.ipynb` (Jupyter Notebook) to generate the query ranking data -> qac_train and qac_test.
-   Use the `Ranking_script.ipynb` to parse the data from the two files generated in step 1 and feed it to a LambdaMart Learn-To-Rank implementation. (Courtesy: [lezzago](https://github.com/lezzago/LambdaMart))
-   We will push the results and a detailed pdf of the project soon.

## Additional information:

LambdaMART API:
LambdaMART(training_data=None, number_of_trees=0, leaves_per_tree=0, learning_rate=0)

Parameters:

	training_data: Numpy array of documents (default: None)
		Each document’s  format is [relevance score, query index, feature vector]
	number_of_trees: int (default: 5)
		Number of trees LambdaMART goes through
	learning_rate: float (default: 0.1)
		Rate at which we update our prediction with each tree
	tree_type: string (default: “sklearn”)
		Either “sklearn” for using Sklearn implementation of the tree or “original” for 
		using our implementation of the tree.

Reproducing results from paper https://www.microsoft.com/en-us/research/wp-content/uploads/2015/10/spir0468-mitra.pdf


Please work on the *develop* branch if required